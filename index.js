// Number

let number = prompt("Please, provide a number");
console.log("The number you provided is " + number);

while(number > 0){
	number--;
	if(number <= 50){
		break;
	}
	if(number % 10 === 0){
		console.log("the number is divisible by 10. Skipping the number");
		continue;
	}
	if(number % 5 === 0){
		console.log(number);
	}
}

// Word

let word = "supercalifragilisticexpialidocious";
let consonants = "";

for(let i = 0; i < word.length; i++){
	if(word[i] === "a" ||
	   word[i] === "e" ||
	   word[i] === "i" ||
	   word[i] === "o" ||
	   word[i] === "u"	
	    ){
		continue;
	}
	else{	
		consonants += word[i];		
	}
}
console.log(word);
console.log(consonants);
